<?php
/**
 * Product form file
 *
 * @author Sergey Zelenov <serwizz@gmail.com>
 */

namespace Product\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Form;

class ProductForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('product');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new \Product\Form\ProductInputFilter());

        $this->add(
            array(
                'name' => 'title',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'required' => 'required',
                ),
                'options' => array(
                    'min' => 3,
                    'max' => 300,
                    'label' => 'Название продукта',
                ),
            )
        );

        $this->add(
            array(
                'name' => 'description',
                'type' => 'Zend\Form\Element\Textarea',
                'attributes' => array(
                    'max' => 2000,
                    'required' => 'required',
                ),
                'options' => array(
                    'label' => 'Описание продукта',
                ),
            )
        );

        $this->add(
            array(
                'name' => 'price',
                'type' => 'Zend\Form\Element\Text',
                'attributes' => array(
                    'required' => 'required',
                ),
                'options' => array(
                    'label' => 'Цена продукта',
                ),
            )
        );

        $this->add(
            array(
                'name' => 'submit',
                'type' => 'Submit',
                'attributes' => array(
                    'value' => 'Сохранить',
                    'id' => 'submitbutton',
                ),
            )
        );
    }
}
