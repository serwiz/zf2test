<?php
/**
 * Product controller file
 *
 * @author Sergey Zelenov <serwizz@gmail.com>
 */

namespace Product\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Product\Entity;

/**
 * Class ProductController
 *
 * @package Product\Controller
 */
class ProductController extends AbstractActionController
{

    /**
     * Главная страница
     *
     * @return array|ViewModel
     */
    public function indexAction()
    {
        $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $products = $om->getRepository('\Product\Entity\Product')->findAll();

        $productsArray = array();
        foreach ($products as $product) {
            $productsArray[] = $product->getArrayCopy();
        }

        $view = new ViewModel(array(
            'products' => $productsArray,
        ));

        return $view;
    }

    /**
     * Подробное описание продукта
     *
     * @return array|ViewModel
     */
    public function viewAction()
    {
        $id = intval($this->params()->fromRoute('id', 0));
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Такой продукт не существует');
            return $this->redirect()->toRoute('product');
        }
        $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $product = $om->getRepository('\Product\Entity\Product')->findOneBy(array('id' => $id));

        if (!$product) {
            $this->flashMessenger()->addErrorMessage(sprintf('Продукт с id %s не существует', $id));
            return $this->redirect()->toRoute('product');
        }

        $view = new ViewModel(array(
            'product' => $product->getArrayCopy(),
        ));

        return $view;
    }

    /**
     * Добавление продукта
     *
     * @return array|\Zend\Http\Response
     */
    public function addAction()
    {
        $form = new \Product\Form\ProductForm();
        $form->get('submit')->setValue('Добавить продукт');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

                $product = new \Product\Entity\Product();

                $product->exchangeArray($form->getData());

                $om->persist($product);
                $om->flush();

                // Redirect to list of products
                return $this->redirect()->toRoute('product');
            }
        }
        return array('form' => $form);
    }

    /**
     * Удаление
     *
     * @return array|\Zend\Http\Response
     */
    public function deleteAction()
    {
        $id = intval($this->params()->fromRoute('id', 0));
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Продукт не существует');
            return $this->redirect()->toRoute('product');
        }

        $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = intval($request->getPost('id'));
                try {
                    $product = $om->find('Product\Entity\Product', $id);
                    $om->remove($product);
                    $om->flush();
                } catch (\Exception $ex) {
                    $this->flashMessenger()->addErrorMessage('Ошибка при удалении продукта');
                    return $this->redirect()->toRoute(
                        'product',
                        array(
                            'action' => 'index'
                        )
                    );
                }

                $this->flashMessenger()->addMessage(sprintf('Продукт %d удалён', $id));
            }

            return $this->redirect()->toRoute('product');
        }

        return array(
            'id' => $id,
            'product' => $om->find('Product\Entity\Product', $id)->getArrayCopy(),
        );
    }

    /**
     * Редактирование продукта
     *
     * @return array|\Zend\Http\Response
     */
    public function editAction()
    {
        // Check if id set.
        $id = intval($this->params()->fromRoute('id', 0));
        if (!$id) {
            $this->flashMessenger()->addErrorMessage('Продукт не задан');
            return $this->redirect()->toRoute('product');
        }

        // Create form.
        $form = new \Product\Form\ProductForm();
        $form->get('submit')->setValue('Изменить продукт');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

                $data = $form->getData();
                try {
                    $product = $om->find('\Product\Entity\Product', $id);
                } catch (\Exception $ex) {
                    $this->flashMessenger()->addErrorMessage('Ошибка изменения продукта');
                    return $this->redirect()->toRoute(
                        'product',
                        array(
                            'action' => 'index'
                        )
                    );
                }

                $product->exchangeArray($form->getData());

                $om->persist($product);
                $om->flush();

                $message = 'Продукт успешно изменён';
                $this->flashMessenger()->addMessage($message);

                // Redirect to list of products
                return $this->redirect()->toRoute('product');
            } else {
                $this->flashMessenger()->addErrorMessage('Ошибка изменения продукта');
                return array('form' => $form, 'id' => $id);
            }
        }

        $om = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $product = $om->getRepository('\Product\Entity\Product')->findOneBy(array('id' => $id));

        if (!$product) {
            $this->flashMessenger()->addErrorMessage(sprintf('Продукт с id %s не сущуствует', $id));
            return $this->redirect()->toRoute('product');
        }

        // Fill form data.
        $form->bind($product);
        return array('form' => $form, 'id' => $id, 'post' => $product);
    }
}



