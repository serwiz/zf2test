<?php
/**
 * Product module config file
 *
 * @author Sergey Zelenov <serwizz@gmail.com>
 */
return array(
    'doctrine' => array(
        'driver' => array(
            'product_entity' => array(
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => array(__DIR__ . '/../src/Product/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Product\Entity' => 'product_entity',
                )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Product\Controller\Product' => 'Product\Controller\ProductController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Product\Controller\Product',
                        'action'     => 'index',
                    ),
                ),
            ),
            'product' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/product[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Product\Controller\Product',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ZfcTwigViewStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'showMessages' => 'Product\View\Helper\ShowMessages',
        ),
    ),
    'translator' => array(
        'locale' => 'ru_RU', // по умолчанию
        'translation_file_patterns' => array(
            array(
                'type'        => 'gettext',
                'base_dir'    => __DIR__ . '/../language',
                'pattern'     => '%s.mo',
                'text_domain' => 'admin'
            ),
            array(
                'type'        => 'phparray',
                'base_dir'    => __DIR__ . '/../resources/languages',
                'pattern'     => '/%s/Zend_Validate.php',
                'text_domain' => 'default'
            )
        )
    ),
);
