<?php

namespace Product\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

class ProductInputFilter extends InputFilter
{
    public function __construct()
    {
        $this->add(
            array(
                'name' => 'title',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'min' => 3,
                            'max' => 300,
                        ),
                    ),
                ),
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),

            )
        );

        $this->add(
            array(
                'name' => 'description',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'max' => 2000,
                        ),
                    ),
                ),
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
            )
        );

        $this->add(
            array(
                'name' => 'price',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Float',
                    ),
                ),
                'filters' => array(
                    array('name' => 'StringTrim'),
                )
            )
        );

    }
}
